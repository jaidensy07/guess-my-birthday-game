from calendar import month
from random import randint

name = input("Hi! What is your name? ")

number_of_guesses = int(input("How many guesses would you like me to have? "))
month = randint(1,12)
year = randint(1924,2004)
response = "no"
count = 1

for i in range(1, number_of_guesses):
    print("Guess", count, ":", name, "were you born in", month, year,)
    response = input("yes or no? ")

    if response.lower() == "yes":
        print("I knew it!")
        exit()

    elif response.lower() == "no":
        count = count + 1
        print("Drat! Lemme try again!")
        month = randint(1,12)
        year = randint(1924,2004)

    else:
        print("I have other things to do. Good bye.")

print("Aw shuchks I didnt guess it ")