from calendar import month
from random import randint

name = input("Hi! What is your name? ")

#month count
low_month = 1
high_month = 12
month = randint(low_month,high_month)

#year count
low_year = 1924
high_year = 2004
year = randint(low_year,high_year)

#day count
low_day = 1
high_day = 31
day = randint(low_day,high_day)

#guess count
count = 1
num_guesses = int(input("How many guesses would you like? "))


#provide a set amount of guesses
while count <= num_guesses:
    print("Guess", count, ":", name, "were you born in", month, day, year,)
    response = input("year later, year earlier? month later, month earlier? day later, day earlier? or yes? ")
    count = count + 1
#exit if correct
    if response.lower() == "yes":
        print("It took", count, "guesses to figure you your birthday!")
        exit()
#adjust year
    elif response.lower() == "year later":
        low_year = year + 1
        year = randint(low_year,high_year)

    elif response.lower() == "year earlier":
        high_year = year - 1
        year = randint(low_year,high_year)
#adjust month
    elif response.lower() == "month later":
        low_month = month + 1
        month = randint(low_month,high_month)

    elif response.lower() == "month earlier":
        high_month = month - 1
        month = randint(low_month,high_month)
#adjust day
    elif response.lower() == "day later":
        low_day = day + 1
        day = randint(low_day,high_day)

    elif response.lower() == "day earlier":
        high_day = day - 1
        day = randint(low_day,high_day)
#if irrelevant answer, state its not helpful  
    else:
        print("well thats not very helpful.")

